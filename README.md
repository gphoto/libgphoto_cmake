# libgphoto_cmake

A CMake port of the libgphoto2 library (https://github.com/gphoto/libgphoto2)

# Tested
tested on OSX and Android using an Nikon Z6

# License
GNU Lesser General Public License v2.1

# Contact
Jonas Scheer <dev@jscheer.de>
