/*
 * Copyright (C) 2020 Jonas Scheer <dev@jscheer.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */

#if !defined (O_BINARY)
	/*To have portable binary open() on *nix and on Windows */
	#define O_BINARY 0
#endif

#include <stdlib.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdarg.h>
#include <string.h>
#include <gphoto2/gphoto2.h>
#include <unistd.h>

#include<iostream>

#include <gphoto2/gphoto2-port.h>
#include <gphoto2/gphoto2-port-library.h>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <vector>


CameraWidget *actualrootconfig = nullptr;
Camera *camera = nullptr;
GPContext *cameraContext = nullptr;

CameraWidget* getCameraWidget(std::vector<char*> widgetPath) {

	int retval;
	CameraWidget *rootconfig; // okay, not really
	CameraWidgetType widgettype;
	retval = gp_camera_get_config(camera, &rootconfig, cameraContext);
	if (retval != GP_OK) {
		printf("GP_ERROR: gp_camera_get_config() failed");
		return nullptr;
	}

	actualrootconfig = rootconfig;

	for(int i=0; i<widgetPath.size(); i++) {
		const char* childName = widgetPath[i];

		CameraWidget *child;
		retval = gp_widget_get_child_by_name(rootconfig, childName, &child);
		if (retval != GP_OK) {
			std::string currentChildPath = "";
			for(int j=0; j<i; j++) {
				currentChildPath.append("/");
				currentChildPath.append(widgetPath[j]);
			}
			currentChildPath.append(childName);

			printf("GP_ERROR: gp_widget_get_child_by_name(%s) failed", currentChildPath.c_str());
			return nullptr;
		}

		gp_widget_get_type(child, &widgettype);
		rootconfig = child;
	}

	return rootconfig;
}

void updateCameraConfig()
{
	if(actualrootconfig == nullptr)
		return;

	gp_camera_set_config(camera, actualrootconfig, cameraContext);
}

void setValue(std::vector<char*> &widgetPath, const char* value)
{
	CameraWidget *widget = getCameraWidget(widgetPath);
	if(widget == nullptr)
		return;
	
	char *current;
	
	int retval = gp_widget_set_value(widget, value);
	if (retval != GP_OK) {
		printf("GP_ERROR: gp_widget_set_value(%s)", value);
	}

	updateCameraConfig();	
}

char* getValue(std::vector<char*> &widgetPath)
{
	CameraWidget *widget = getCameraWidget(widgetPath);
	if(widget == nullptr)
		return nullptr;
		
	char *current;

	int retval = gp_widget_get_value(widget, &current);
	if (retval != GP_OK) {
		printf("GP_ERROR: gp_widget_get_value()");
		return nullptr;
	}

	return current;
}

std::vector<char*> get_options(std::vector<char*> widgetPath)
{	
	int retval;
	std::vector<char*> options;

	CameraWidget *widget = getCameraWidget(widgetPath);
	if(widget == nullptr)
		return options;

	int choises = gp_widget_count_choices(widget);
	if(choises > 0) 
	{
		for(int i=0; i<choises; i++) {

			char *choice;
			retval = gp_widget_get_choice(widget, i, (const char **)&choice);			 // TODO
			if(retval == GP_OK) {
				options.push_back(choice);
				// printf("\nmy choise: %s\n", options[i]);
			}
		}
	}

	return options;
}

// ISO

std::vector<char*> get_path_iso()
{
	std::vector<char*> settingsTree;
	settingsTree.push_back("main");
	settingsTree.push_back("imgsettings");
	settingsTree.push_back("iso");
	return settingsTree;
}

char* get_iso() {

	std::vector<char*> settingsTree = get_path_iso();
	return getValue(settingsTree);
}

void set_iso(const char* value) {
	std::vector<char*> settingsTree = get_path_iso();
	setValue(settingsTree, value);
}

std::vector<char*> get_iso_options() {

	std::vector<char*> settingsTree = get_path_iso();
	std::vector<char*> options = get_options(settingsTree);
	return options;
}

// shutterspeed

std::vector<char*> get_path_shutterspeed()
{
	std::vector<char*> settingsTree;
	settingsTree.push_back("main");
	settingsTree.push_back("capturesettings");
	settingsTree.push_back("shutterspeed");
	return settingsTree;
}

char* get_shutterspeed() {

	std::vector<char*> settingsTree = get_path_shutterspeed();
	return getValue(settingsTree);
}

void set_shutterspeed(const char* value) {
	std::vector<char*> settingsTree = get_path_shutterspeed();
	setValue(settingsTree, value);
}

std::vector<char*> get_iso_shutterspeed() {

	std::vector<char*> settingsTree = get_path_shutterspeed();
	std::vector<char*> options = get_options(settingsTree);
	return options;
}

// Aperture

std::vector<char*> get_path_aperture()
{
	std::vector<char*> settingsTree;
	settingsTree.push_back("main");
	settingsTree.push_back("capturesettings");
	settingsTree.push_back("f-number");
	return settingsTree;
}

char* get_aperture() {

	std::vector<char*> settingsTree = get_path_aperture();
	return getValue(settingsTree);
}

void set_aperture(const char* value) {
	std::vector<char*> settingsTree = get_path_aperture();
	setValue(settingsTree, value);
}

std::vector<char*> get_aperture_options() {

	std::vector<char*> settingsTree = get_path_aperture();
	std::vector<char*> options = get_options(settingsTree);
	return options;
}

// manualfocusdrive

std::vector<char*> get_path_manualfocusdrive()
{
	std::vector<char*> settingsTree;
	settingsTree.push_back("main");
	settingsTree.push_back("actions");
	settingsTree.push_back("manualfocusdrive");
	return settingsTree;
}

char* get_manualfocusdrive() {

	std::vector<char*> settingsTree = get_path_manualfocusdrive();
	return getValue(settingsTree);
}

void set_manualfocusdrive(const char* value) {
	std::vector<char*> settingsTree = get_path_manualfocusdrive();
	setValue(settingsTree, value);
}

// std::vector<char*> get_aperture_options() {

// 	std::vector<char*> settingsTree = get_path_manualfocusdrive();
// 	std::vector<char*> options = get_options(settingsTree);
// 	return options;
// }

// focusmode

std::vector<char*> get_path_focusmode()
{
	std::vector<char*> settingsTree;
	settingsTree.push_back("main");
	settingsTree.push_back("capturesettings");
	settingsTree.push_back("focusmode");
	return settingsTree;
}

char* get_focusmode() {

	std::vector<char*> settingsTree = get_path_focusmode();
	return getValue(settingsTree);
}

void set_focusmode(const char* value) {
	std::vector<char*> settingsTree = get_path_focusmode();
	setValue(settingsTree, value);
}

// std::vector<char*> get_focusmode_options() {

// 	std::vector<char*> settingsTree = get_path_focusmode();
// 	std::vector<char*> options = get_options(settingsTree);
// 	return options;
// }

// Focal Length

std::vector<char*> get_path_focallength()
{
	std::vector<char*> settingsTree;
	settingsTree.push_back("main");
	settingsTree.push_back("capturesettings");
	settingsTree.push_back("focallength");
	return settingsTree;
}

char* get_focallength() {

	std::vector<char*> settingsTree = get_path_focallength();
	return getValue(settingsTree);
}

// void set_focallength(const char* value) {

// 	std::vector<char*> settingsTree = get_path_focallength();
// 	setValue(settingsTree, value);
// }

// Capture Target

void set_capturetarget(const char* captureTarget) {
	int retval;

	std::vector<char*> settingsTree;
	settingsTree.push_back("main");
	settingsTree.push_back("settings");
	settingsTree.push_back("capturetarget");

	setValue(settingsTree, captureTarget);	
}

int getImages(const char* startFolder, std::vector<std::string> &imgNames)
{
	int retval;
	CameraList *list;
	retval = gp_list_new (&list);
	retval = gp_camera_folder_list_files(camera, startFolder, list, cameraContext); 
	if(retval != GP_OK) {
		gp_list_free (list);
		return retval;
	}

	int count = gp_list_count (list);
	for (int i = 0; i < count; i++) {
		const char *name;
		gp_list_get_name (list, i, &name);
		std::string imgName(name);
		imgNames.push_back(imgName);
	}

	gp_list_free (list);
	return (GP_OK);
}


int getFolders(const char* startFolder, std::vector<std::string> &folders, bool recursive)
{
	int retval;
	CameraList *list;
	retval = gp_list_new (&list);
	retval = gp_camera_folder_list_folders(camera, startFolder, list, cameraContext); 
	if(retval != GP_OK) {
		gp_list_free (list);
		return retval;
	}

	int count = gp_list_count (list);
	for (int i = 0; i < count; i++) {
		const char *name;
		gp_list_get_name (list, i, &name);
		std::string folderName(name);
		std::string fullFolderPath(startFolder);
		std::string lastChar = fullFolderPath.substr(fullFolderPath.length()-1, fullFolderPath.length());
		if(lastChar.compare("/") != 0) {
			fullFolderPath.append("/");
		}
		fullFolderPath.append(folderName);
		folders.push_back(fullFolderPath);
		if(recursive) {
			getFolders(fullFolderPath.c_str(), folders, recursive);
		}
	}

	gp_list_free (list);
	return (GP_OK);
}

int getAllImages(char*** folderNames, char*** imgNames, int* imgCount) {

	std::vector<std::string> folderNamesStr;
	std::vector<std::string> imgNamesStr;
	
	std::vector<std::string> folders;
	int retval = getFolders("/", folders, true);
	if(retval != GP_OK) {
		return retval;
	}

	for(int i=0; i<folders.size(); i++) {

		std::vector<std::string> imgs; 
		retval = getImages(folders[i].c_str(), imgs);
		if(retval == GP_OK) {
			for(int j=0; j<imgs.size(); j++) {

				folderNamesStr.push_back(folders[i]);
				imgNamesStr.push_back(imgs[j]);
			}
		}
	}

	*imgCount = folderNamesStr.size();
	*folderNames = (char**)malloc(sizeof(char*) * *imgCount);
	*imgNames = (char**)malloc(sizeof(char*) * *imgCount);
	for(int i=0; i<folderNamesStr.size(); i++) {
		size_t len;

		len = strlen(folderNamesStr[i].c_str());
		printf("len: %d\n", len);
		char* folderName = (char*)malloc(sizeof(char)*len);
		strcpy(folderName, folderNamesStr[i].c_str());
		(*folderNames)[i] = folderName;
		len = strlen(folderNamesStr[i].c_str());
		char* imgName = (char*)malloc(sizeof(char)*len);
		strcpy(imgName, imgNamesStr[i].c_str());
		(*imgNames)[i] = imgName;
	}


	return 0;
}


int main(int argc, char **argv)
{
	int	retval;

    // allocate memory for new camera
    retval = gp_camera_new(&camera);
		if (retval != GP_OK) {
  		printf("  Retval of gp_camera_new: %d\n", retval);
  		exit (1);
  	}

    // init camera
    printf("Camera init.  Takes about 10 seconds.\n");
  	cameraContext = gp_context_new();
	retval = gp_camera_init_libusb(camera, cameraContext);

  	if (retval != GP_OK) {
  		printf("  ERROR: Retval of gp_camera_init: %d\n", retval);
  		exit (1);
  	}

	const char* target = "Memory card";
	set_capturetarget(target);

    // preview
    char	*data;
	unsigned long size;
	CameraFile *file;

	retval = gp_file_new(&file);
	if (retval != GP_OK) {
		fprintf(stderr,"gp_file_new: %d\n", retval);
		exit(1);
	}

	int* imgCount = new int;
	// std::vector<std::string> imgNames;
	// std::vector<std::string> folderNames;
	char*** imgNames = (char***)malloc(sizeof(char**));
	char*** folderNames = (char***)malloc(sizeof(char**));
	getAllImages(folderNames, imgNames, imgCount);
	for(int imgN=0; imgN<*imgCount; imgN++) {
		printf("[%04d] : %s -- %s\n", imgN, (*folderNames)[imgN], (*imgNames)[imgN]);
	}

	// Downliad image tests
	CameraFile *downloadFile;
	char	*downloadedData;
	unsigned long downloadedSize;
	retval = gp_file_new(&downloadFile);
	
	CameraFileType type = GP_FILE_TYPE_PREVIEW;
	// CameraFileType type = GP_FILE_TYPE_NORMAL;
	retval = gp_camera_file_get(camera, (*folderNames)[*imgCount-1], (*imgNames)[*imgCount-1], type, downloadFile, cameraContext);
	if (retval != GP_OK) {
		fprintf(stderr,"gp_camera_file_get: %d\n", retval);
		exit(1);
	}
	retval = gp_file_get_data_and_size (downloadFile,  (const char**)&downloadedData, &downloadedSize);
	if (retval != GP_OK) {
		fprintf(stderr,"gp_file_get_data_and_size: %d\n", retval);
		exit(1);
	}
	else {
		printf("WORKS : size: %l", downloadedSize);

		cv::Mat frame;
		if (downloadedSize > 0)
		{
			cv::Mat buf = cv::Mat(1, downloadedSize, CV_8UC1, (void *) downloadedData);
			if(!buf.empty())
			{
				frame = imdecode(buf, cv::IMREAD_UNCHANGED);
			}
		}

		imshow( "Display window", frame );
		cv::waitKey(0);
	}
	//




	printf(" Start Preview\n");
	int fps = 24;
    for(int i=0; i<fps*10; i++)
    {

      retval = gp_camera_capture_preview(camera, file, cameraContext);
  		if (retval != GP_OK) {
  			fprintf(stderr,"gp_camera_capture_preview(%d): %d\n", i, retval);
  			exit(1);
  		}

		if(i == fps*4) {
			gp_camera_trigger_capture(camera, cameraContext);
		}

		if(i == fps*3) {
			// get_iso_options();
			printf("get_aperture: %s", get_aperture());
			//printf("get_focallength: %s", get_focallength());  // not working, yet.
			printf("get_iso: %s", get_iso());
			printf("get_shutterspeed: %s", get_shutterspeed());
			
			set_iso("800");
			set_aperture("f/2.2");

			set_shutterspeed("0.0200s");
		}

		if(i == fps*6) {
			gp_camera_trigger_capture(camera, cameraContext);
		}

		int stat = gp_file_get_data_and_size (file, (const char**)&data, &size);

		cv::Mat frame;
		if (size > 0)
		{
			cv::Mat buf = cv::Mat(1, size, CV_8UC1, (void *) data);
			if(!buf.empty())
			{
				frame = imdecode(buf, cv::IMREAD_UNCHANGED);
			}
		}

		imshow( "Display window", frame );
		cv::waitKey(1);
    } // end for-loop

	set_capturetarget("Internal RAM");
  	gp_camera_exit(camera, cameraContext);

    return 0;
}
