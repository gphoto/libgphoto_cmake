//
//  CaptureManager.m
//  GPhotoTestApp
//
//  Created by Jonas Scheer on 30.07.20.
//  Copyright © 2020 Jonas Scheer. All rights reserved.
//

#import "CaptureManager.h"
//#import "CaptureManagerInterface.h"
#define PTP_RC_OK_OSX                       0x2001


void setPtpSendCallback(int (*ptpSendCallback)(ICCameraDevice*, unsigned char*, unsigned char*, int, int), ICCameraDevice* icCameraDevice);
void setPtpResonseCallback(int (*ptpResponseCallback)( unsigned char*, unsigned char*));
void setResetPtpResonseCallback(int (*resetPtpResponseCallback)());
void setCheckPtpResonseCallback(int (*checkPtpResponseCallback)(int*, int*));
int initUsb(int (*ptpSendCallback)(const char*, int),int usbVendorID, int usbProductID, int usbLocationID);

ICCameraDevice* _camera = nil;

dispatch_semaphore_t ptpResponseSemaphore;

NSMutableData* responseCommand = nil;
NSMutableData* responseData = nil;
NSInteger responseLength = 0;
NSInteger dataLength = 0;

@implementation CaptureManager

- (instancetype)init {
    if ((self = [super init])) {
    }
    
    return self;
}

- (void)close {
    [_deviceBrowser setDelegate:nil];
    [_deviceBrowser stop];
}

- (void)deviceDidBecomeReady:(ICDevice *)device {
}

- (void)deviceBrowser:(ICDeviceBrowser*)browser
         didAddDevice:(ICDevice*)addedDevice
           moreComing:(BOOL)moreComing {
    
    if([addedDevice.productKind isEqualToString:@"Camera"]) {
        
        if ( [addedDevice.capabilities containsObject:ICCameraDeviceCanAcceptPTPCommands] )
        {
           ICCameraDevice* camera = (ICCameraDevice *) addedDevice;
           camera.delegate = self;
           [camera requestOpenSession];
            
           _camera = camera;
            
            setPtpSendCallback(externPtpCommand, _camera);
            setPtpResonseCallback(getPtpResponse);
            setCheckPtpResonseCallback(checkPtpResponse);
            setResetPtpResonseCallback(resetPtpResponse);
            
            ptpResponseSemaphore = dispatch_semaphore_create(0);
        }
    }
}

- (void)deviceBrowser:(nonnull ICDeviceBrowser *)browser didRemoveDevice:(nonnull ICDevice *)device moreGoing:(BOOL)moreGoing {
}


- (void)device:(nonnull ICDevice *)device didCloseSessionWithError:(NSError * _Nullable)error {
    NSLog(@"READY: didCloseSessionWithError");
}


- (void)device:(nonnull ICDevice *)device didOpenSessionWithError:(NSError * _Nullable)error {
    
    if ( error )
        NSLog(@"\nFailed to open a session on '%@'.\nError: '%@' \n", device.name, error.description);
    else
        NSLog(@"\nSession opened on '%@'.\n", device.name);
}


- (void)didRemoveDevice:(nonnull ICDevice *)device {
    if([device.productKind isEqualToString:@"Camera"]) {
        {
        }
    }
}

static int externPtpCommand(ICCameraDevice* icCameraDevice, unsigned char* command, unsigned char* data, int commandSize, int dataSize)
{
    // Prepare data to be send using requestSendPTPCommand()
    NSData* sendCommand = [NSData dataWithBytesNoCopy:command length:commandSize freeWhenDone:NO];
    NSData* sendData = [NSData dataWithBytesNoCopy:data length:dataSize freeWhenDone:NO];

    [icCameraDevice requestSendPTPCommand:sendCommand outData:sendData completion:^(NSData* d1, NSData* d2, NSError* err){

        
        dataLength = d1.length;
        responseLength = d2.length;
        
        // ensure that storage is allocated and can hold enough data
        if(responseCommand == nil) {
           responseCommand = [NSMutableData dataWithLength:sizeof(unsigned char) * 12];
        }
        if(responseCommand.length < responseLength )
        {
            [responseCommand setLength:sizeof(unsigned char) * responseLength];
        }
        
        if(responseData == nil) {
           responseData = [NSMutableData dataWithLength:sizeof(unsigned char) * 4096];
        }
        if(responseData.length < dataLength )
        {
            [responseData setLength:sizeof(unsigned char) * dataLength];
        }
        
        // set data
        [responseCommand setData:d2];
        [responseData setData:d1];
        
        dispatch_semaphore_signal(ptpResponseSemaphore);
    }];
    
    return commandSize;
}

static int checkPtpResponse(int* commandSize, int* dataSize)
{
    if (![NSThread isMainThread]) {
        dispatch_semaphore_wait(ptpResponseSemaphore, DISPATCH_TIME_FOREVER);
    } else {
        while (dispatch_semaphore_wait(ptpResponseSemaphore, DISPATCH_TIME_NOW)) {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:0]];
        }
    }
    
    *commandSize = (int)responseLength;
    *dataSize = (int)dataLength;
    
    return PTP_RC_OK_OSX;
}

static int getPtpResponse(unsigned char* command, unsigned char* data)
{
    memcpy(command, responseCommand.bytes, responseLength);
    memcpy(data, responseData.bytes, dataLength);
    
    return PTP_RC_OK_OSX;
}

static int resetPtpResponse()
{
    dispatch_semaphore_signal(ptpResponseSemaphore);
    return PTP_RC_OK_OSX;
}

- (void)cameraDevice:(nonnull ICCameraDevice *)camera didAddItems:(nonnull NSArray<ICCameraItem *> *)items {
}

- (void)cameraDevice:(nonnull ICCameraDevice *)camera didReceiveMetadata:(NSDictionary * _Nullable)metadata forItem:(nonnull ICCameraItem *)item error:(NSError * _Nullable)error {
}

- (void)cameraDevice:(nonnull ICCameraDevice *)camera didReceivePTPEvent:(nonnull NSData *)eventData {
    NSLog(@"READY: didReceivePTPEvent");
}

- (void)cameraDevice:(nonnull ICCameraDevice *)camera didReceiveThumbnail:(CGImageRef _Nullable)thumbnail forItem:(nonnull ICCameraItem *)item error:(NSError * _Nullable)error {
}

- (void)cameraDevice:(nonnull ICCameraDevice *)camera didRemoveItems:(nonnull NSArray<ICCameraItem *> *)items {
}

- (void)cameraDevice:(nonnull ICCameraDevice *)camera didRenameItems:(nonnull NSArray<ICCameraItem *> *)items {
}

- (void)cameraDeviceDidChangeCapability:(nonnull ICCameraDevice *)camera {
}

- (void)cameraDeviceDidEnableAccessRestriction:(nonnull ICDevice *)device {
}

- (void)cameraDeviceDidRemoveAccessRestriction:(nonnull ICDevice *)device {
}

- (void)deviceDidBecomeReadyWithCompleteContentCatalog:(nonnull ICCameraDevice *)device {
    
    if(device != _camera)
        return;
    
    int usbVendor = _camera.usbVendorID;
    int usbProduct = _camera.usbProductID;
    initUsb(NULL, usbVendor, usbProduct, _camera.usbLocationID);
    
    [_delegate cameraStarted];
}

-(void)startCamera
{
    _deviceBrowser = [[ICDeviceBrowser alloc] init];
    _deviceBrowser.delegate = self;
    
    [_deviceBrowser start];
}

@end
