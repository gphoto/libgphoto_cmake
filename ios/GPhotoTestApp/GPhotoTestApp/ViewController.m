//
//  ViewController.m
//  GPhotoTestApp
//
//  Created by Jonas Scheer on 30.07.20.
//  Copyright © 2020 Jonas Scheer. All rights reserved.
//

#import "ViewController.h"
#import "CaptureManager.h"
#include <gphoto2/gphoto2-camera.h>

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // create and start capture manager
    _manager = [[CaptureManager alloc] init];
    _manager.delegate = self;
    [_manager startCamera];
}


- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

#pragma mark - CaptureManagerDelegate
-(void)cameraStarted {
    
    NSLog(@"Camera Example");
    
    Camera* mCamera;
    GPContext* mContext;
    CameraFile* mFile;
    
    int retval = gp_camera_new(&mCamera);
    mContext = gp_context_new();
    retval = gp_camera_init_libusb(mCamera, mContext);
    retval = gp_file_new(&mFile);
    
    // simple camera trigger
    retval = gp_camera_trigger_capture (mCamera, mContext);
    
    
    gp_camera_exit(mCamera, mContext);
    
    NSLog(@"camera Stuff Finished");
}


@end
