//
//  AppDelegate.h
//  GPhotoTestApp
//
//  Created by Jonas Scheer on 30.07.20.
//  Copyright © 2020 Jonas Scheer. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

