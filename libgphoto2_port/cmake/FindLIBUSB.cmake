#  CMakeLists.txt
#
#  Copyright 2020 Jonas Scheer <dev@jscheer.de>
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the
#  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA  02110-1301  USA
#
# Looks for Libusb using PkgConfig
# This modules defines
#  LIBUSB_FOUND


if (${CMAKE_SYSTEM_NAME} STREQUAL "Android")



  set(USB_INCLUDE_DIRS ${GPHOTO_DIR}/include/libusb-1.0)
  message("use android USB_INCLUDE_DIRS: " ${USB_INCLUDE_DIRS})

  set(LIBUSB_DIR ${GPHOTO_DIR}/android/libs/${ANDROID_ABI})
  link_directories(${LIBUSB_DIR})
  set(LIBUSB usb1.0)
  message("use android LIBUSB: " ${LIBUSB_DIR}/${LIBUSB})

else()

  find_package(PkgConfig REQUIRED)
  pkg_check_modules(USB REQUIRED libusb-1.0)
  if(USB_FOUND)
    #include_directories(${USB_INCLUDE_DIRS})
    set(LIBUSB ${USB_LIBRARIES})
    link_directories(${USB_LIBRARY_DIRS})

    set(LIBUSB_FOUND True)
  else()
    message( FATAL_ERROR "No LIBUSB found." )
  endif()
endif()

include_directories(${USB_INCLUDE_DIRS})
