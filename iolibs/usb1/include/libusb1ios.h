/* libusb1ios.h
 *
 * Copyright 2020 Jonas Scheer <dev@jscheer.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */
#ifndef LIBUSB_IOS_H
#define LIBUSB_IOS_H

 #ifdef __APPLE__
 #include "TargetConditionals.h"
 #if TARGET_OS_IPHONE && TARGET_IPHONE_SIMULATOR
      // define something for simulator
 #elif TARGET_OS_IPHONE
    // define something for iphone


#include <libusb.h>         // libusb-1.0 from github
#include <stdio.h>

extern __attribute__((visibility("default"))) __attribute((used)) int initUsb(int usbVendorID, int usbProductID, int usbLocationID);

#endif	// TARGET_OS_IPHONE
#endif	// __APPLE__
#endif //LIBUSB_IOS_H
