/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* gphoto2-port-usb.c
 *
 * Copyright 2001 Lutz Mueller <lutz@users.sf.net>
 * Copyright 1999-2000 Johannes Erdfelt <johannes@erdfelt.com>
 * Copyright 2011,2015 Marcus Meissner <marcus@jet.franken.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */
#include <gphoto2-port-library.h>

static int gp_libusb1_init (GPPort *port);
static int gp_libusb1_exit (GPPort *port);
static int gp_libusb1_open (GPPort *port);
static int gp_libusb1_close (GPPort *port);
static int gp_libusb1_read(GPPort *port, char *bytes, int size);
static int gp_libusb1_reset(GPPort *port);
static int gp_libusb1_write (GPPort *port, const char *bytes, int size);
static int gp_libusb1_check_int (GPPort *port, char *bytes, int size, int timeout);
static int gp_libusb1_update (GPPort *port);
static int gp_libusb1_clear_halt_lib(GPPort *port, int ep);
static int gp_libusb1_msg_write_lib(GPPort *port, int request, int value, int index, char *bytes, int size);
static int gp_libusb1_msg_read_lib(GPPort *port, int request, int value, int index, char *bytes, int size);
static int gp_libusb1_msg_interface_write_lib(GPPort *port, int request, int value, int index, char *bytes, int size);
static int gp_libusb1_msg_interface_read_lib(GPPort *port, int request, int value, int index, char *bytes, int size);
static int gp_libusb1_msg_class_write_lib(GPPort *port, int request, int value, int index, char *bytes, int size);
static int gp_libusb1_msg_class_read_lib(GPPort *port, int request, int value, int index, char *bytes, int size);
static int gp_libusb1_find_device_lib(GPPort *port, int idvendor, int idproduct);
static int gp_libusb1_find_device_by_class_lib(GPPort *port, int class, int subclass, int protocol);



GPPortOperations *
gp_port_library_operations (void)
{
	GPPortOperations *ops;

	ops = calloc (1, sizeof (GPPortOperations));
	if (!ops)
		return (NULL);

	ops->init   = gp_libusb1_init;
	ops->exit   = gp_libusb1_exit;
	ops->open   = gp_libusb1_open;
	ops->close  = gp_libusb1_close;
	ops->read   = gp_libusb1_read;
	ops->reset  = gp_libusb1_reset;
	ops->write  = gp_libusb1_write;
	ops->check_int = gp_libusb1_check_int;
	ops->update = gp_libusb1_update;
	ops->clear_halt = gp_libusb1_clear_halt_lib;
	ops->msg_write  = gp_libusb1_msg_write_lib;
	ops->msg_read   = gp_libusb1_msg_read_lib;
	ops->msg_interface_write  = gp_libusb1_msg_interface_write_lib;
	ops->msg_interface_read   = gp_libusb1_msg_interface_read_lib;
	ops->msg_class_write  = gp_libusb1_msg_class_write_lib;
	ops->msg_class_read   = gp_libusb1_msg_class_read_lib;
	ops->find_device = gp_libusb1_find_device_lib;
	ops->find_device_by_class = gp_libusb1_find_device_by_class_lib;

	return (ops);
}
