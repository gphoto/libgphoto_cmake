/* libusb1JNI.h
 *
 * Copyright 2020 Jonas Scheer <dev@jscheer.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */

#ifdef __ANDROID__

#ifndef LIBUSB_JNI_H
#define LIBUSB_JNI_H

#include <jni.h>
#include <libusb.h>         // libusb-1.0 from github

JNIEXPORT int JNICALL Java_com_example_GPhoto2UsbInterface_GPhoto2UsbInterface_initUsbWithId(JNIEnv * env, jobject obj, int usbVendorID, int usbProductID, int usbLocationID);

JNIEXPORT int JNICALL Java_com_example_GPhoto2UsbInterface_GPhoto2UsbInterface_initUsb(JNIEnv * env, jobject obj, jobject gp2, jint sys_dev);

static intptr_t libusb_descriptor = NULL;

// JAVA VM. Can be used to get a JNIEnv in an arbitrary thread
static JavaVM* jvm;

// temporary variables to communicate with JAVA classes, methods or objects
static jclass cl_GPhoto2;
static jobject gGp2;
static jmethodID m_GPhoto2_bulkTransferOut, m_GPhoto2_bulkTransferIn;

// byteArray for sending data to JAVA fom C++
static jbyteArray data_in, data_out;

static const char* classPath = "com/example/GPhoto2UsbInterface/GPhoto2UsbInterface";
static const char* methodBlkTransfOut = "bulkTransfereOut";
static const char* methodBlkTransfOutSig = "([BI)I";
static const char* methodBlkTransfIn = "bulkTransfereIn";
static const char* methodBlkTransfInSig  = "([BII)I";

static int mVendorId = -1;
static int mProductId = -1;
static int mUsbLocation = -1;


#endif //LIBUSB_JNI_H
#endif  // Android
