/* usb.c
 *
 * Copyright (C) 2001-2004 Mariusz Woloszyn <emsi@ipartners.pl>
 * Copyright (C) 2003-2016 Marcus Meissner <marcus@jet.franken.de>
 * Copyright (C) 2003-2017 Marcus Meissner <marcus@jet.franken.de>
 * Copyright (C) 2006-2007 Linus Walleij <triad@df.lth.se>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */

#ifndef __GPHOTO2_USB_IOS_H__
#define __GPHOTO2_USB_IOS_H__

#define _DEFAULT_SOURCE
#include <config.h>
#include "ptp.h"
#include "ptp-private.h"
#include "ptp-bugs.h"

#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include <gphoto2/gphoto2-library.h>
#include <gphoto2/gphoto2-port-log.h>
#include <gphoto2/gphoto2-setting.h>
//
//#ifdef ENABLE_NLS
//#  include <libintl.h>
//#  undef _
//#  define _(String) dgettext (PACKAGE, String)
//#  ifdef gettext_noop
//#    define N_(String) gettext_noop (String)
//#  else
//#    define N_(String) (String)
//#  endif
//#else
//#  define textdomain(String) (String)
//#  define gettext(String) (String)
//#  define dgettext(Domain,Message) (Message)
//#  define dcgettext(Domain,Message,Type) (Message)
//#  define bindtextdomain(Domain,Directory) (Domain)
//#  define _(String) (String)
//#  define N_(String) (String)
//#endif
//
//#define CONTEXT_BLOCK_SIZE	200000
//
//#define PTP_CNT_INIT(cnt) {memset(&cnt,0,sizeof(cnt));}
//
///* PTP2_FAST_TIMEOUT: how long (in milliseconds) we should wait for
// * an URB to come back on an interrupt endpoint */
///* 100 is not enough for various cameras types. 150 seems to work better */
//#define PTP2_FAST_TIMEOUT       150

/* Pack / unpack functions */

#include "ptp-pack.c"
#include "ICCameraDeviceInterface.h"

/* send / receive functions */

uint16_t
ptp_usb_sendreq (PTPParams* params, PTPContainer* req, int dataphase)
{
    int        len = 12+req->Nparam*4;
    unsigned char     *request = malloc(len);

    htod32a(&request[0],len);
    /* sending data = 2, receiving data or no data = 1 */
    if ((dataphase&PTP_DP_DATA_MASK) == PTP_DP_SENDDATA)
        htod16a(&request[4],2);
    else
        htod16a(&request[4],1);

    htod16a(&request[6],req->Code);
    //htod16a(&request[6],PTP_OC_GetDeviceInfo);
    htod32a(&request[8],req->Transaction_ID);

    switch (req->Nparam) {
    case 5: htod32a(&request[12+(4*4)],req->Param5);
    case 4: htod32a(&request[12+(3*4)],req->Param4);
    case 3: htod32a(&request[12+(2*4)],req->Param3);
    case 2: htod32a(&request[12+(1*4)],req->Param2);
    case 1: htod32a(&request[12],req->Param1);
    case 0:
    default:
        break;
    }

    int res = __PtpSendCallback(__icCameraDevice, request, NULL, len, 0);

    free (request);

    return PTP_RC_OK;
}

uint16_t
ptp_usb_senddata (PTPParams* params, PTPContainer* ptp,
          uint64_t size, PTPDataHandler *handler
) {
  // build PTP communicated
  int        len = 12+ptp->Nparam*4;
  unsigned char     *request = malloc(len);

  htod32a(&request[0],len);
  htod16a(&request[4], 2); // sending data = 2, receiving data or no data = 1

  htod16a(&request[6],ptp->Code);
  htod32a(&request[8],ptp->Transaction_ID);

  switch (ptp->Nparam) {
  case 5: htod32a(&request[12+(4*4)],ptp->Param5);
  case 4: htod32a(&request[12+(3*4)],ptp->Param4);
  case 3: htod32a(&request[12+(2*4)],ptp->Param3);
  case 2: htod32a(&request[12+(1*4)],ptp->Param2);
  case 1: htod32a(&request[12],ptp->Param1);
  case 0:
  default:
      break;
  }

  // Build data container
  unsigned char *bytes = malloc (size);
  unsigned long readlen;

  uint16_t ret = handler->getfunc (params, handler->priv, size, bytes, &readlen);
  int res = __PtpSendCallback(__icCameraDevice, request, bytes, len, size);

  free (request);
  free(bytes);

  res = __ResetPtpResponseCallback();

  return PTP_RC_OK;
}

uint16_t
ptp_usb_getdata (PTPParams* params, PTPContainer* ptp, PTPDataHandler *handler)
{
    uint32_t        rlen;
    uint16_t         ret;
    int ptpDataSize = 0;

    // get ptp response or data from osx/ios
    ret = __CheckPtpResponseCallback(&rlen, &ptpDataSize);

    unsigned char* ptpCommand = malloc(rlen);
    unsigned char* ptpData = malloc(ptpDataSize);
    ret = __PtpResponseCallback(ptpCommand, ptpData);


    /* Copy the payload bytes we already read (with the first usb packet) */
  	ret = handler->putfunc (params, handler->priv, ptpDataSize, ptpData);

    free(ptpCommand);
    free(ptpData);

    ret = __ResetPtpResponseCallback();

    return PTP_RC_OK;
}

uint16_t
ptp_usb_getresp (PTPParams* params, PTPContainer* resp)
{
    uint32_t		rlen;
    uint16_t 		ret;
    int ptpDataSize = 0;

    // get ptp response or data from osx/ios
    ret = __CheckPtpResponseCallback(&rlen, &ptpDataSize);

    unsigned char* ptpCommand = malloc(rlen);
    unsigned char* ptpData = malloc(ptpDataSize);
    ret = __PtpResponseCallback(ptpCommand, ptpData);
    uint32_t numParams = (rlen-12) >> 2;

	if (ret!=PTP_RC_OK) {
		ret = PTP_ERROR_IO;
	} else
	if (rlen < 12) {
		ret = PTP_ERROR_IO;
	}

	if (ret!=PTP_RC_OK) {
		GP_LOG_E ("PTP_OC 0x%04x receiving resp failed: %s (0x%04x)", resp->Code, ptp_strerror(ret, params->deviceinfo.VendorExtensionID), ret);
		return ret;
	}
	/* build an appropriate PTPContainer */
	resp->Code = dtoh16(*((uint16_t*)(ptpCommand+6)));
    resp->SessionID=params->session_id;
	resp->Transaction_ID=dtoh32(*((uint32_t*)(ptpCommand+8)));
	resp->Nparam=(rlen-12)/4;


    switch (resp->Nparam) {
    case 5: resp->Param5=dtoh32(*((uint32_t*)(ptpCommand+28)));
    case 4: resp->Param4=dtoh32(*((uint32_t*)(ptpCommand+24)));
    case 3: resp->Param3=dtoh32(*((uint32_t*)(ptpCommand+20)));
    case 2: resp->Param2=dtoh32(*((uint32_t*)(ptpCommand+16)));
    case 1: resp->Param1=dtoh32(*((uint32_t*)(ptpCommand+12)));
    case 0:
    default:
        break;
    }

	if (resp->Transaction_ID != params->transaction_id - 1) {
		resp->Transaction_ID=params->transaction_id-1;
	}

    free(ptpCommand);
    free(ptpData);

    return ret;
}




/*
 --------------------------------------------------------------------------------
 The following code was not changed, yet.
 We can integrate this on demand in the future.
*/




/* Event handling functions */

#define PTP_CNT_INIT(cnt) {memset(&cnt,0,sizeof(cnt));}

/* PTP2_FAST_TIMEOUT: how long (in milliseconds) we should wait for
 * an URB to come back on an interrupt endpoint */
/* 100 is not enough for various cameras types. 150 seems to work better */
#define PTP2_FAST_TIMEOUT       150

/* PTP Events wait for or check mode */
#define PTP_EVENT_CHECK			0x0000	/* waits for */
#define PTP_EVENT_CHECK_FAST		0x0001	/* checks */
#define PTP_EVENT_CHECK_QUEUE		0x0002	/* just looks in the queue */

static inline uint16_t
ptp_usb_event (PTPParams* params, PTPContainer* event, int wait)
{
	int			result, timeout, fasttimeout;
	unsigned long		rlen;
	PTPUSBEventContainer	usbevent;
	Camera			*camera = ((PTPData *)params->data)->camera;

	if (params->deviceinfo.VendorExtensionID == PTP_VENDOR_CANON)
		fasttimeout = PTP2_FAST_TIMEOUT*2;
	else
		fasttimeout = PTP2_FAST_TIMEOUT;

	PTP_CNT_INIT(usbevent);

	if (event==NULL)
		return PTP_ERROR_BADPARAM;

	switch(wait) {
	case PTP_EVENT_CHECK:
		result = gp_port_check_int (camera->port, (char*)&usbevent, sizeof(usbevent));
		if (result <= 0) result = gp_port_check_int (camera->port, (char*)&usbevent, sizeof(usbevent));
		break;
	case PTP_EVENT_CHECK_FAST:
		gp_port_get_timeout (camera->port, &timeout);
		gp_port_set_timeout (camera->port, fasttimeout);
		result = gp_port_check_int (camera->port, (char*)&usbevent, sizeof(usbevent));
		if (result <= 0) result = gp_port_check_int (camera->port, (char*)&usbevent, sizeof(usbevent));
		gp_port_set_timeout (camera->port, timeout);
		break;
	case PTP_EVENT_CHECK_QUEUE:
		gp_port_get_timeout (camera->port, &timeout);
		gp_port_set_timeout (camera->port, 0); /* indicates no waiting at all */
		result = gp_port_check_int (camera->port, (char*)&usbevent, sizeof(usbevent));
		gp_port_set_timeout (camera->port, timeout);
		break;
	default:
		return PTP_ERROR_BADPARAM;
	}
	if (result < 0) {
		if ((result != GP_ERROR_TIMEOUT) || (wait != PTP_EVENT_CHECK_FAST))
			GP_LOG_E ("Reading PTP event failed: %s (%d)", gp_port_result_as_string(result), result);
		return translate_gp_result_to_ptp(result);
	}
	if (result == 0) {
		GP_LOG_E ("Reading PTP event failed: a 0 read occurred, assuming timeout.");
		return PTP_ERROR_TIMEOUT;
	}
	rlen = result;
	if (rlen < 8) {
		GP_LOG_E ("Reading PTP event failed: only %ld bytes read", rlen);
		return PTP_ERROR_IO;
	}

	/* Only do the additional reads for "events". Canon IXUS 2 likes to
	 * send unrelated data.
	 */
	if (	(dtoh16(usbevent.type) == PTP_USB_CONTAINER_EVENT) &&
		(dtoh32(usbevent.length) > rlen)
	) {
		GP_LOG_D ("Canon incremental read (done: %ld, todo: %d)", rlen, dtoh32(usbevent.length));
		gp_port_get_timeout (camera->port, &timeout);
		gp_port_set_timeout (camera->port, PTP2_FAST_TIMEOUT);
		while (dtoh32(usbevent.length) > rlen) {
			result = gp_port_check_int (camera->port, ((char*)&usbevent)+rlen, sizeof(usbevent)-rlen);
			if (result <= 0)
				break;
			rlen += result;
		}
		gp_port_set_timeout (camera->port, timeout);
	}
	/* if we read anything over interrupt endpoint it must be an event */
	/* build an appropriate PTPContainer */
	event->Nparam  = (rlen-12)/4;
	event->Code   = dtoh16(usbevent.code);
	event->SessionID=params->session_id;
	event->Transaction_ID=dtoh32(usbevent.trans_id);
	event->Param1 = dtoh32(usbevent.param1);
	event->Param2 = dtoh32(usbevent.param2);
	event->Param3 = dtoh32(usbevent.param3);
	return PTP_RC_OK;
}

uint16_t
ptp_usb_event_check_queue (PTPParams* params, PTPContainer* event) {

	return ptp_usb_event (params, event, PTP_EVENT_CHECK_QUEUE);
}

uint16_t
ptp_usb_event_check (PTPParams* params, PTPContainer* event) {

	return ptp_usb_event (params, event, PTP_EVENT_CHECK_FAST);
}

uint16_t
ptp_usb_event_wait (PTPParams* params, PTPContainer* event) {

	return ptp_usb_event (params, event, PTP_EVENT_CHECK);
}

uint16_t
ptp_usb_control_get_extended_event_data (PTPParams *params, char *buffer, int *size) {
	Camera	*camera = ((PTPData *)params->data)->camera;
	int	ret;

	GP_LOG_D ("Getting extended event data.");
	ret = gp_port_usb_msg_class_read (camera->port, 0x65, 0x0000, 0x0000, buffer, *size);
	if (ret < GP_OK)
		return PTP_ERROR_IO;
	*size = ret;
	return PTP_RC_OK;
}

uint16_t
ptp_usb_control_device_reset_request (PTPParams *params) {
	Camera	*camera = ((PTPData *)params->data)->camera;
	int	ret;

	GP_LOG_D ("Sending usb device reset request.");
	ret = gp_port_usb_msg_class_write (camera->port, 0x66, 0x0000, 0x0000, NULL, 0);
	if (ret < GP_OK)
		return PTP_ERROR_IO;
	return PTP_RC_OK;
}

uint16_t
ptp_usb_control_get_device_status (PTPParams *params, char *buffer, int *size) {
	Camera	*camera = ((PTPData *)params->data)->camera;
	int	ret;

	ret = gp_port_usb_msg_class_read (camera->port, 0x67, 0x0000, 0x0000, buffer, *size);
	if (ret < GP_OK)
		return PTP_ERROR_IO;
	*size = ret;
	return PTP_RC_OK;
}

uint16_t
ptp_usb_control_cancel_request (PTPParams *params, uint32_t transactionid) {
	Camera	*camera = ((PTPData *)params->data)->camera;
	int	ret;
	unsigned char	buffer[6];

	htod16a(&buffer[0],PTP_EC_CancelTransaction);
	htod32a(&buffer[2],transactionid);
	ret = gp_port_usb_msg_class_write (camera->port, 0x64, 0x0000, 0x0000, (char*)buffer, sizeof (buffer));
	if (ret < GP_OK)
		return PTP_ERROR_IO;
	return PTP_RC_OK;
}

#endif // #ifndef __GPHOTO2_USB_IOS_H__
