//
//  CaptureManagerInterface.h
//  GPhotoTestApp
//
//  Created by Jonas Scheer on 01.08.20.
//  Copyright © 2020 Jonas Scheer. All rights reserved.
//

#ifndef ICCameraDeviceInterface_h
#define ICCameraDeviceInterface_h

extern __attribute__((visibility("default"))) __attribute((used)) int (*__PtpSendCallback)(void*, unsigned char*, unsigned char*, int, int) = NULL;

extern __attribute__((visibility("default"))) __attribute((used)) int (*__PtpResponseCallback)(unsigned char*, unsigned char*) = NULL;

extern __attribute__((visibility("default"))) __attribute((used)) int (*__CheckPtpResponseCallback)(int*, int*) = NULL;

extern __attribute__((visibility("default"))) __attribute((used)) int (*__ResetPtpResponseCallback)() = NULL;


extern __attribute__((visibility("default"))) __attribute((used)) void* __icCameraDevice = NULL;

extern __attribute__((visibility("default"))) __attribute((used)) void setPtpSendCallback(int (*ptpSendCallback)(void*, unsigned char*, unsigned char*, int, int), void* icCameraDevice) {
    __PtpSendCallback = ptpSendCallback;
    __icCameraDevice = icCameraDevice;
};


extern __attribute__((visibility("default"))) __attribute((used)) void setPtpResonseCallback(int (*ptpResponseCallback)( unsigned char*, unsigned char*))
{
    __PtpResponseCallback = ptpResponseCallback;
}

extern __attribute__((visibility("default"))) __attribute((used)) void setCheckPtpResonseCallback(int (*checkPtpResponseCallback)(int*, int*))
{
    __CheckPtpResponseCallback = checkPtpResponseCallback;
}

extern __attribute__((visibility("default"))) __attribute((used)) void setResetPtpResonseCallback(int (*resetPtpResponseCallback)())
{
    __ResetPtpResponseCallback = resetPtpResponseCallback;
}
    

#endif /* ICCameraDeviceInterface */
